import java.util.Scanner;
class numbers2 {
    public static void main (String [] args) {

        System.out.println("input number from 1 to 3");
        Scanner inputFigure = new Scanner(System.in);
        int number = inputFigure.nextInt();
        switch (number) {
            case 1 -> System.out.println("result - 1");
            case 2 -> System.out.println("result - 2");
            case 3 -> System.out.println("result - 3");
            default -> System.out.println("error");
        }
    }
}